# retorna todos os valores do hash ao quadrado

def pow_hash(key)
    to_return = []
    key.each do |item|
        to_return.append(item[1] ** 2)
    end
    return to_return
end

#puts pow_hash({:chave1 => 5, :chave2 => 30, :chave3 => 20})