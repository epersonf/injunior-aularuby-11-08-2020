# soma todos os elementos de uma matriz

def array_sum_mult(array)
    # 0 = elemento neutro da adicao
    sum = 0
    # 1 = elemento neutro da multiplicacao
    mult = 1
    #percorre todos os elementos
    for i in array
        for j in i
        sum += j
        mult *= j
        end
    end
    puts "Soma: #{sum}"
    puts "Multiplicacao: #{mult}"
end

#array_sum_mult([[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]])