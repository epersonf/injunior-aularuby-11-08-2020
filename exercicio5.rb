# retorna um array com todos os elementos divisiveis por n

def divisible(array, n=3)
    to_return = []
    for i in array
        unless (i % n == 0)
            next
        end
        #append em retorno
        to_return.append(i)
    end
    return to_return
end

#puts divisible([3, 6, 7, 8])